package shop.dev.linwise.api.kakao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import shop.dev.linwise.api.kakao.enums.KakaoUri;

import shop.dev.linwise.api.kakao.model.ConvertAddressResponse;
import shop.dev.linwise.api.kakao.model.SearchAddressByKeywordResponse;
import shop.dev.linwise.api.kakao.model.SearchAddressResponse;
import shop.dev.linwise.api.kakao.model.SearchDistrictResponse;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@SpringBootTest
public class ApiKakaoApplicationTests {
    @Test
    void contextLoads() {

    }
    @Test
    void apiCallTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_ADDRESS.getApiSubUri();

        String addressValue = "고잔로 88";
        String queryString = "?query=" + URLEncoder.encode(addressValue, StandardCharsets.UTF_8);

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 99ed3a6e3660c29a9da4cc3d91a353d1");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchAddressResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressResponse.class);
        SearchAddressResponse result = responseEntity.getBody();


        int a = 1;

    }
    @Test
    void apiDistrictCallTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_DISTRICT.getApiSubUri();

        Double coorXvalue = 127.423084873712;
        Double coorYvalue = 37.0789561558879;

        String coorString = "?x=" + coorXvalue + "&y=" + coorYvalue;

        URI uri = URI.create(apiFullUri + coorString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "KakaoAK 99ed3a6e3660c29a9da4cc3d91a353d1");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchDistrictResponse> responseEntity = restTemplate.exchange(requestEntity, SearchDistrictResponse.class);

        SearchDistrictResponse result = responseEntity.getBody();
        int a = 1;
    }

    @Test
    void apiChangeCoordToAddressTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.COOR_TO_ADDRESS.getApiSubUri();

        Double coorXvalue = 127.423084873712;
        Double coorYvalue = 37.0789561558879;
        String inputCoord = "&input_coord=WGS84";


        String coorString = "?x=" + coorXvalue + "&y=" + coorYvalue;

        URI uri = URI.create(apiFullUri + coorString + inputCoord);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "KakaoAK 99ed3a6e3660c29a9da4cc3d91a353d1");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<ConvertAddressResponse> responseEntity = restTemplate.exchange(requestEntity, ConvertAddressResponse.class);

        ConvertAddressResponse result = responseEntity.getBody();
        int a = 1;

    }

    @Test
    void apiChangeKeyWordToAddressTest() {

        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.KEYWORD_TO_ADDRESS.getApiSubUri();

        String keyWordValue = "카카오프렌즈";
        String queryString = "?query=" + URLEncoder.encode(keyWordValue, StandardCharsets.UTF_8);
        String radiusValue = "&radius=20000";


        URI uri = URI.create(apiFullUri + queryString + radiusValue);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "KakaoAK 99ed3a6e3660c29a9da4cc3d91a353d1");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchAddressByKeywordResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressByKeywordResponse.class);

        SearchAddressByKeywordResponse result = responseEntity.getBody();

        int a = 1;

    }

    @Test
    void apiSearchAddressByCategoryTest() {
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.CATEGORY_TO_ADDRESS.getApiSubUri();

        String categoryCodeValue = "?category_group_code=PM9";
        String radiusValue = "&radius=20000";
        URI uri = URI.create(apiFullUri + categoryCodeValue + radiusValue);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "KakaoAK 99ed3a6e3660c29a9da4cc3d91a353d1");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchAddressByKeywordResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressByKeywordResponse.class);

        SearchAddressByKeywordResponse result = responseEntity.getBody();

        int a = 1;
    }
}
