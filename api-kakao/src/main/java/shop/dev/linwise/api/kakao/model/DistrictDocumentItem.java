package shop.dev.linwise.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistrictDocumentItem {
    @ApiModelProperty(value = "H(행정동) 또는 B(법정동)")
    private String region_type;
    @ApiModelProperty(value = "지역 1Depth, 시도 단위")
    private String region_1depth_name;

    @ApiModelProperty(value = "지역 2Depth, 구 단위")
    private String region_2depth_name;

    @ApiModelProperty(value = "지역 3Depth, 동 단위")
    private String region_3depth_name;




}
