package shop.dev.linwise.api.kakao.members.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NearFriendSearchRequest {
    @ApiModelProperty(notes = "내 위치 x값.. 126머시기..")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "내 위치 x값.. 37머시기..")
    @NotNull
    private Double posY;

    @ApiModelProperty(notes = "조회할 거리")
    @NotNull
    private Integer distance;
}
