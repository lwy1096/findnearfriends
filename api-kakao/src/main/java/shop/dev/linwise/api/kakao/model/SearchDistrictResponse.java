package shop.dev.linwise.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SearchDistrictResponse {
    @ApiModelProperty(notes = "결과 아이템들")
    private List<DistrictDocumentItem> documents;

}
