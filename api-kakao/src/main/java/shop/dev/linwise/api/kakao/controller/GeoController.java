package shop.dev.linwise.api.kakao.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.linwise.api.kakao.model.SearchAddressResponse;
import shop.dev.linwise.api.kakao.service.GeoService;
import shop.dev.linwise.common.response.model.SingleResult;
import shop.dev.linwise.common.response.service.ResponseService;

@Api(tags = "좌표 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/geo")
public class GeoController {
    private final GeoService geoService;

    @ApiOperation(value = "주소를 좌표로 변경하기")
    @GetMapping("/search/address")
    public SingleResult<SearchAddressResponse> getSearchAddress(@RequestParam("searchAddress") String searchAddress) {
        return ResponseService.getSingleResult(geoService.getSearchAddress(searchAddress));
    }


}
