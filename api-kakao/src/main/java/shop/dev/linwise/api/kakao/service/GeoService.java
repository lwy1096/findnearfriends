package shop.dev.linwise.api.kakao.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import shop.dev.linwise.api.kakao.enums.KakaoUri;
import shop.dev.linwise.api.kakao.lib.RestApi;
import shop.dev.linwise.api.kakao.model.ConvertAddressResponse;
import shop.dev.linwise.api.kakao.model.SearchAddressResponse;
import shop.dev.linwise.api.kakao.model.SearchDistrictResponse;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Service
public class GeoService {
    @Value("${kakao.api.domain}")
    String KAKAO_API_DOMAIN;

    @Value(("${kakao.api.rest-key}"))
    String KAKAO_API_REST_KEY;

    public SearchAddressResponse getSearchAddress(String searchAddress) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.SEARCH_ADDRESS.getApiSubUri();
        String queryString = "?query=" + URLEncoder.encode(searchAddress, StandardCharsets.UTF_8);

        String resultUrl = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, resultUrl, KAKAO_API_REST_KEY, SearchAddressResponse.class);
    }
    public SearchDistrictResponse getSearchRegion(String x, String y) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.SEARCH_DISTRICT.getApiSubUri();
        String queryString = "?x=" + x + "&y=" + y;

        String resultUrl = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, resultUrl, KAKAO_API_REST_KEY, SearchDistrictResponse.class);
    }

    public ConvertAddressResponse getConvertAddress(String x, String y) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.COOR_TO_ADDRESS.getApiSubUri();

        String coord = "WGS84";
        String queryString = "?x=" + x + "&y=" + y + "&input_coord=" + coord;

        String resultUrl = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, resultUrl, KAKAO_API_REST_KEY, ConvertAddressResponse.class);
    }

}
