package shop.dev.linwise.api.kakao.members;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiKakaoMembersApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiKakaoMembersApplication.class, args);
    }
}
