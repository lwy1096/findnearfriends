package shop.dev.linwise.api.kakao.members;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import shop.dev.linwise.api.kakao.members.entity.Member;
import shop.dev.linwise.api.kakao.members.repository.MemberRepository;

import java.util.List;

@SpringBootTest
public class ApiMemberApplicationTests {

    @Autowired
    MemberRepository memberRepository;

    @Test
    void contextLoads() {

    }
    @Test
    void dataTest1() {
        double searchX = 126.835639929462;
        double searchY = 37.3179417691029;
        int searchDistance = 5000;

        List<Member> result = memberRepository.findAllFriends(searchY, searchX, searchDistance);

        int a = 1;

    }
    @Test
    void dataTest2() {
        double searchX = 126.835639929462;
        double searchY = 37.3179417691029;
        int searchDistance = 5000;
        String gender = "WOMAN";

        List<Member> result = memberRepository.findAllFriendsByGender(searchY, searchX, searchDistance, gender);

        int a = 1;
    }

}
