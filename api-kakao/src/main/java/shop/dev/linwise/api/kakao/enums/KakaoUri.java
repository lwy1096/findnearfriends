package shop.dev.linwise.api.kakao.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum KakaoUri {

    SEARCH_ADDRESS("주소 검색하기", "/v2/local/search/address.json")
    ,SEARCH_DISTRICT("행정구역 검색하기","/v2/local/geo/coord2regioncode.json")
    ,COOR_TO_ADDRESS("좌표로 주소 변환하기", "/v2/local/geo/coord2address.json")
    ,KEYWORD_TO_ADDRESS("키워드로 장소 검색하기", "/v2/local/search/keyword.json")
    ,CATEGORY_TO_ADDRESS("카테고리로 장소 검색하기", "/v2/local/search/category.json")
    ;
    private final String apiName;
    private final String apiSubUri;

}
