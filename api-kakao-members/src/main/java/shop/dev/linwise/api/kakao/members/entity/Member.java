package shop.dev.linwise.api.kakao.members.entity;

import shop.dev.linwise.api.kakao.members.enums.Gender;
import shop.dev.linwise.api.kakao.members.model.MemberCreateRequest;
import shop.dev.linwise.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "닉네임")
    @Column(nullable = false, length = 20)
    private String nickname;

    @ApiModelProperty(notes = "취미")
    @Column(nullable = false, length = 50)
    private String hobby;

    @ApiModelProperty(notes = "성별")
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "위치 x좌표")
    @Column(nullable = false)
    private Double posX;

    @ApiModelProperty(notes = "위치 y좌표")
    @Column(nullable = false)
    private Double posY;

    private Member(Builder builder) {
        this.nickname = builder.nickname;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.posX = builder.posX;
        this.posY = builder.posY;
    }

    public static class Builder implements CommonModelBuilder<Member> {
        private final String nickname;
        private final String hobby;
        private final Gender gender;
        private final Double posX;
        private final Double posY;

        public Builder(MemberCreateRequest createRequest) {
            this.nickname = createRequest.getNickname();
            this.hobby = createRequest.getHobby();
            this.gender = createRequest.getGender();
            this.posX = createRequest.getPosX();
            this.posY = createRequest.getPosY();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
