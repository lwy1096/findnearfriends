
# 동네친구 찾기 API 🙌
* ### 카카오API를 연동하여 GPS(위도,경도)기술을 사용,동네 친구를 찾을 수 있게 하는 서비스 API입니다.

## 제작기간
> ### 2023.03.06 ~ 2023.03.10


<hr/>

## 사용 기술 🔎


<img src="https://img.shields.io/badge/java 17-006600?style=for-the-badge&logoColor=fff"/>
<img src="https://img.shields.io/badge/JPA -ECD53F?style=for-the-badge&logoColor=fff"/>
<img src="https://img.shields.io/badge/postgres-4169E1?style=for-the-badge&logoColor=fff"/>


## 기능 🔎
```
    1. java api를 이용한 주소 -> 좌표 변환 및 검색 
    2. java api를 이용한 좌표 -> 주소 변환 및 검색
```

## 기획 의도
> __카카오 API의 연동과 함께, 주변 친구들에 대한 정보를 조회하고,<br/> 타 회사의 API를 연동하는 방법을
사전에 파악하고, 반영하기 위함__