package shop.dev.linwise.api.kakao.members.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.dev.linwise.api.kakao.members.model.MemberCreateRequest;
import shop.dev.linwise.api.kakao.members.model.NearFriendItem;
import shop.dev.linwise.api.kakao.members.model.NearFriendSearchRequest;
import shop.dev.linwise.api.kakao.members.service.MemberService;
import shop.dev.linwise.common.response.model.CommonResult;
import shop.dev.linwise.common.response.model.ListResult;
import shop.dev.linwise.common.response.service.ResponseService;

import javax.validation.Valid;


@Api(tags = "동네 친구 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "동네 친구 등록")
    @PostMapping("/friend")
    public CommonResult setMember(@RequestBody @Valid MemberCreateRequest createRequest) {
        memberService.setMember(createRequest);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "내 근처 친구 조회")
    @PostMapping("/search/friends") // 기능은 R(Get)이지만 body를 쓰기 위해 PostMapping 으로 정의함
    public ListResult<NearFriendItem> getFriends(@RequestBody @Valid NearFriendSearchRequest searchRequest) { // PostMapping 이기때문에 body로 값 받기 가능
                                                // memberService.getNearFriends가 받아야 할 값을 표시함
        return ResponseService.getListResult(memberService.getNearFriends(searchRequest.getPosX(), searchRequest.getPosY(), searchRequest.getDistance()), true);
    }

}