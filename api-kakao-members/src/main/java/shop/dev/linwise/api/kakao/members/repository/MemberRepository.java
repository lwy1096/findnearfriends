package shop.dev.linwise.api.kakao.members.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import shop.dev.linwise.api.kakao.members.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {

    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX )) <= :searchDistance"

            ,nativeQuery = true
    )
    List<Member> findAllFriends(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance);

    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX )) <= :searchDistance and member.gender = :gender"
            ,nativeQuery = true
    )
    List<Member> findAllFriendsByGender(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance, String gender);
}
