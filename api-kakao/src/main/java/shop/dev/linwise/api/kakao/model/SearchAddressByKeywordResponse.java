package shop.dev.linwise.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SearchAddressByKeywordResponse {
    private List<KeyWordConvertToAdressitem> documents;
}
