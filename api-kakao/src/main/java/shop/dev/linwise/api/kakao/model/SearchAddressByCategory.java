package shop.dev.linwise.api.kakao.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchAddressByCategory {
    @ApiModelProperty(value = "장소명, 업체명")
    private String place_name;
    @ApiModelProperty(value = "카테고리 이름")
    private String category_name;
    @ApiModelProperty(value = "전화번호")
    private String phone;
    @ApiModelProperty(value = "전체 지번 주소")
    private String address_name;
    @ApiModelProperty(value = "전체 도로명 주소")
    private String road_address_name;
    @ApiModelProperty(value = "중심좌표까지의 거리 (단, x,y 파라미터를 준 경우에만 존재) 단위 meter")
    private String distance;


}
