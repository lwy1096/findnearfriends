package shop.dev.linwise.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

