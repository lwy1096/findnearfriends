package shop.dev.linwise.api.kakao.members.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.linwise.api.kakao.members.enums.Gender;
import shop.dev.linwise.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NearFriendItem {
    @ApiModelProperty(notes = "닉네임")
    private String nickname;
    @ApiModelProperty(notes = "취미")
    private String hobby;
    @ApiModelProperty(notes = "성별(enum)")
    private String gender;
    @ApiModelProperty(notes = "성별(한글명)")
    private String genderName;
    @ApiModelProperty(notes = "나와의 거리")
    private Double distanceM;


    private NearFriendItem(NearFriendITemBuilder builder) {
        this.nickname = builder.nickname;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.genderName = builder.genderName;
        this.distanceM = builder.distanceM;

    }
    public static class NearFriendITemBuilder implements CommonModelBuilder<NearFriendItem> {

        private final String nickname;
        private final String hobby;
        private final String gender;
        private final String genderName;
        private final Double distanceM;

        public NearFriendITemBuilder(String nickname, String hobby, String gender, Double distanceM){

            this.nickname = nickname;
            this.hobby = hobby;
            this.gender = gender;
            this.genderName = Gender.valueOf(gender).getName();
            this.distanceM = distanceM;

        }

        @Override
        public NearFriendItem build() {
            return new NearFriendItem(this);
        }
    }
}
