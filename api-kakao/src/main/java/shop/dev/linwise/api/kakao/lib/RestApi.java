package shop.dev.linwise.api.kakao.lib;

import com.google.gson.Gson;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import shop.dev.linwise.common.exception.CMissingDataException;

import java.net.URI;

public class RestApi {
    public static <T> T callApi(HttpMethod httpMethod, String apiUrl, String restKey, Class<T> responseModel) {
        try {
            URI uri = URI.create(apiUrl); // String으로 받은 url을 URI 객체로 바꿔주기

            RestTemplate restTemplate = new RestTemplate(); // api call 할 전화기 준비

            // 헤더준비
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "KakaoAK " + restKey);
            headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
            headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");
            // 헤더준비 끝

            RequestEntity<String> requestEntity = new RequestEntity<>(headers, httpMethod, uri); // api call 하기 위해 필요한 데이터 세팅
            String responseText = restTemplate.exchange(requestEntity, String.class).getBody(); // api에 필요한 데이터(request) 넘겨주면서 필요한 데이터 바꿔먹기.. 일단 String으로 바꿔먹는다.

            Gson gson = new Gson(); // json 형태의 string을 원하는 모델로 바꿔주기 위해 이 기능을 할 수 있는 Gson 이란 놈 한명 불러오기

            return gson.fromJson(responseText, responseModel); // gson한테 Class<T> responseModel로 받은..(이 모양으로 바꿔줘) class 모양으로 바꿔달라고 하기

        } catch (Exception e) { // 위에 try 안에 코드 실행하다가 안되면.. 안될때 : api 주소가 잘못되었거나 api 호출 횟수가 소진되었거나 없는키거나..
            e.printStackTrace(); // 에러메세지 로그로 띄워주고
            throw new CMissingDataException(); // advice를 통해서 커스텀 비상구로 밀어버리기
        }
    }
}

