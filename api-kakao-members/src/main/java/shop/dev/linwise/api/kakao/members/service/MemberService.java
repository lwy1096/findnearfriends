package shop.dev.linwise.api.kakao.members.service;

import shop.dev.linwise.api.kakao.members.entity.Member;
import shop.dev.linwise.api.kakao.members.model.MemberCreateRequest;
import shop.dev.linwise.api.kakao.members.model.NearFriendItem;
import shop.dev.linwise.api.kakao.members.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.linwise.common.response.model.ListResult;
import shop.dev.linwise.common.response.service.ListConvertService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;


    @PersistenceContext
    EntityManager entityManager;

    public void setMember(MemberCreateRequest createRequest) {
        Member member = new Member.Builder(createRequest).build();
        memberRepository.save(member);
    }

    /**
     * 근처 친구 리스트를 가져온다.
     *
     * @param posX ex 126.머시기....
     * @param posY ex 37.머시기....
     * @param distanceValue 몇km의 친구 리스트를 가져올지.. ex 1, 2, 3
     * @return
     */

    public ListResult<NearFriendItem> getNearFriends(double posX, double posY, int distanceValue) {
        double distanceResult = distanceValue * 1000; // 미터기준으로 변환 * 1000

        String queryString = "select * from public.get_near_friends(" +posX + "," + posY +"," + distanceResult +")";
        Query nativeQuery = entityManager.createNativeQuery(queryString);

        List<Object[]> resultList = nativeQuery.getResultList();

        List<NearFriendItem> result = new LinkedList<>();
        for(Object[] resultItem : resultList) {
            result.add(
                new NearFriendItem.NearFriendITemBuilder(
                                    resultItem[0].toString(),
                                    resultItem[1].toString(),
                                    resultItem[2].toString(),
                                    Double.parseDouble(resultItem[3].toString())
                ).build());
        }
        return ListConvertService.settingResult(result);

    }
}
